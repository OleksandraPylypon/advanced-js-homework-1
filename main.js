class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }
  get name() {
    return this._name;
  }
  set name(value) {
    this._name = value;
  }

  get age() {
    return this._age;
  }
  set age(value) {
    this._age = value;
  }

  get salary() {
    return this._salary;
  }
  set salary(value) {
    this._salary = value;
  }
}
const employeeObject = new Employee("Oleksandra", 21, 1000);

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }
  get salary() {
    return super.salary * 3;
  }
}
const programmerObject = new Programmer("Oleksandra", 21, 1000, "Ukr");

const programmerObject1 = new Programmer("Viktoria", 19, 500, "Eng");

const programmerObject2 = new Programmer("Oleksiy", 25, 1500, "FR");
console.log(programmerObject, programmerObject1, programmerObject2);
